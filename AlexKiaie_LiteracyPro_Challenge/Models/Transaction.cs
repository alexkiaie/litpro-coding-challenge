﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlexKiaie_LiteracyPro_Challenge.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string Payee { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string Memo { get; set; }

        [ForeignKey("CategoryName")]
        public virtual Category Category { get; set; }
        public string CategoryName { get; set; }
    }
}