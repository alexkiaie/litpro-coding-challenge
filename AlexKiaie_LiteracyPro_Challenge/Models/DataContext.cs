﻿using System.Data.Entity;

namespace AlexKiaie_LiteracyPro_Challenge.Models
{
    public class DataContext : DbContext
    {
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}