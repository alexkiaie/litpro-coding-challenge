namespace AlexKiaie_LiteracyPro_Challenge.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Payee = c.String(),
                        Amount = c.Double(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Memo = c.String(),
                        CategoryName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryName)
                .Index(t => t.CategoryName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "CategoryName", "dbo.Categories");
            DropIndex("dbo.Transactions", new[] { "CategoryName" });
            DropTable("dbo.Transactions");
            DropTable("dbo.Categories");
        }
    }
}
