namespace AlexKiaie_LiteracyPro_Challenge.Migrations
{
    using AlexKiaie_LiteracyPro_Challenge.Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "AlexKiaie_LiteracyPro_Challenge.Models.DataContext";
        }

        protected override void Seed(DataContext context)
        {
            context.Categories.AddOrUpdate(c => c.Name, new Category { Name = "Groceries" });
            context.Categories.AddOrUpdate(c => c.Name, new Category { Name = "Entertainment" });
            context.Categories.AddOrUpdate(c => c.Name, new Category { Name = "Mortgage" });
            context.Categories.AddOrUpdate(c => c.Name, new Category { Name = "Bills" });
            context.Categories.AddOrUpdate(c => c.Name, new Category { Name = "Dining" });
            context.SaveChanges();
        }
    }
}
