﻿using System.Web;
using System.Web.Optimization;

namespace AlexKiaie_LiteracyPro_Challenge
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/kendojquery").Include(
                        "~/Scripts/kendo/js/jquery.min.js",
                        "~/Scripts/kendo/js/kendo.ui.core.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/kendo/kendo.common.min.css",
                      "~/Content/kendo/kendo.default.min.css",
                      "~/Content/site.css"));
        }
    }
}
