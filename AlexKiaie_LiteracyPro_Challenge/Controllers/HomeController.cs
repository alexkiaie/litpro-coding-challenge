﻿using System.Web.Mvc;
using AlexKiaie_LiteracyPro_Challenge.Models;

namespace AlexKiaie_LiteracyPro_Challenge.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}