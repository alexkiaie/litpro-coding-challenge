﻿$("#categoryInput").kendoDropDownList({
    dataTextField: "Name",
    dataValueField: "Name",
    dataSource: {
        transport: {
            read: {
                dataType: "json",
                url: "/api/Categories",
            }
        }
    }
});