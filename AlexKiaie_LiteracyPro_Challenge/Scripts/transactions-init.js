﻿var apiUrl = "/api/Transactions";

var transactionsDataSource = new kendo.data.DataSource({
    schema: {
        model: {
            id: "Id",
            fields: {
                Id: {
                    editable: false,
                    nullable: false,
                    defaultValue: 0
                },
                CategoryName: { type: "string" },
                Payee: { type: "string" },
                Amount: { type: "number" },
                Date: { type: "date" },
                Memo: { type: "string" }
            }
        }
    },
    sort: { field: "Date", dir: "desc" },
    transport: {
        read: {
            url: apiUrl,
            dataType: "json",
            contentType: "application/json"
        },
        update: {
            url: apiUrl,
            type: "PUT",
            dataType: "json",
            contentType: "application/json"
        },
        destroy: {
            url: apiUrl,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json"
        },
        create: {
            url: apiUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json"
        },
        parameterMap: function (options, operation) {
            if (operation !== "read" && options) {
                return JSON.stringify(options);
            }
        }
    },
    aggregate: [
        { field: "Amount", aggregate: "sum" },
        { field: "Amount", aggregate: "average" }
    ],
    change: function (e) {
        $(".transaction-count").html(this.data().length);
        var amountAggregates = this.aggregates().Amount;
        $(".amount-total").html(kendo.toString(amountAggregates.sum, "c"));
        $(".amount-avg").html(kendo.toString(amountAggregates.average, "c"));
    }
});

var listView = $("#listView").kendoListView({
    dataSource: transactionsDataSource,
    template: kendo.template($("#viewTemplate").html()),
    editTemplate: kendo.template($("#editTemplate").html())
}).data("kendoListView");

$(".k-add-button").click(function (e) {
    console.log('akjsdhfkajd')
    listView.add();
    e.preventDefault();
});